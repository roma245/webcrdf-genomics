#!/bin/bash

cd ${3}
mkdir -p data_prokka

#  Make annotation using prokka
${5}prokka ${2} --force --outdir data_prokka/${1} --prefix ${1}

#  Make pretty result file
cd ../..
python3 ${4}scripts/pretty_annotation_result.py ${3}data_prokka/${1} ${1}

#  Create zip archive
cd ${3}data_prokka
zip -rj ${1}.zip ${1}

#  Delete directory
rm -r ${1}
