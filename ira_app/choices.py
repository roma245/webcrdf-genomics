from enum import Enum


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return tuple((x.name, x.value) for x in cls)


class StatusEnum(ChoiceEnum):
    WAITING = 'Waiting'
    PROCEEDING = 'Proceeding'
    SUCCEED = 'Succeed'
    FAILED = 'Failed'


class OperationEnum(ChoiceEnum):
    RESISTANCE = 'Resistance'
    ANNOTATION = 'Annotation'
