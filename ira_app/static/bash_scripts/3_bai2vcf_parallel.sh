#!/bin/bash

cd ${2}
mkdir -p data_vcf

${5}java -Xmx5g -jar ${4}'pilon-1.22.jar' \
        --genome ${3}'reference/H37Rv_NC000962.3_Reference.fasta' \
        --frags ''${2}'data_bam/'${1}'_sorted.bam' \
        --output ''${2}'data_vcf/'${1} --vcf

#  Can merge and then filter, or filter and then merge.
#  Best to filter then merge due to differences in coverage among individual genomes.

#  Compress and index the .vcf files:
${6}bgzip -f ''${2}'data_vcf/'${1}'.vcf'
${7}tabix -p vcf ''${2}'data_vcf/'${1}'.vcf.gz'

#  Filter the genome files to remove SNPs that are not 'PASS' and duplicates and the INFO fields:
${8}vcftools --gzvcf ''${2}'data_vcf/'${1}'.vcf.gz' --remove-filtered-all \
             --remove-INFO IMPRECISE --recode \
             --out ''${2}'data_vcf/'${1}'_filter'

#${6}bgzip 'data_vcf/'${1}'_filter.recode.vcf'
#${7}tabix -p vcf 'data_vcf/'${1}'_filter.recode.vcf.gz'
