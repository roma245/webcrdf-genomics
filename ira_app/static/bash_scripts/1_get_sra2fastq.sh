#!/bin/bash

mkdir -p ${2}data_fastq
cd ${2}data_fastq
${3}fastq-dump --split-files ${1} -v -O ''
