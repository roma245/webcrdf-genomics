import json
import requests
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAdminUser


from .settings import DAG_URL
from .serializers import *
from .permissions import ReadOnly


class UserProfileViewSet(viewsets.ModelViewSet):
    """
    UserProfiles resource.
    ---
    list:
        Return a list of all userprofiles
    retrieve:
        Return the userprofile by id.
    create:
        Create a new userprofile.
    destroy:
        Delete an userprofile.
    update:
        Update an userprofile.
    partial_update:
        Update an userprofile.
    """

    permission_classes = (IsAdminUser | (AllowAny & ReadOnly),)
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()
    filter_backends = (DjangoFilterBackend, filters.SearchFilter,)
    filter_fields = ('user',)
    search_fields = ('user',)

    action_serializers = {
        'list': UserProfileListSerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super(UserProfileViewSet, self).get_serializer_class()


class JobViewSet(viewsets.ModelViewSet):
    """
    Jobs resource.
    ---
    list:
        Return a list of all jobs
    retrieve:
        Return the job by id.
    create:
        Create a new job.
    destroy:
        Delete an job.
    update:
        Update an job.
    partial_update:
        Update an job.
    """

    permission_classes = (IsAdminUser | (AllowAny & ReadOnly),)
    serializer_class = JobSerializer
    queryset = Job.objects.all()
    filter_backends = (DjangoFilterBackend, filters.SearchFilter,)
    filter_fields = ('name', 'sra_id', 'status',)
    search_fields = ('name', 'sra_id', 'status',)

    action_serializers = {
        'list': JobListSerializer,
        'create': JobCreateSerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super(JobViewSet, self).get_serializer_class()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = {}
        if isinstance(request.user, User):
            data = {'user': request.user}
        job = serializer.save(**data)
        response_headers = {'Status': 'Job was successfully started!'}
        if job.sra_id:
            job.func_stack = 0
        else:
            job.func_stack = 3
        job.save()
        params = json.dumps({'conf': {'job_id': job.pk}})
        headers = {
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/json'
        }
        requests.post(url=DAG_URL, data=params, headers=headers)
        return Response(serializer.data, status=status.HTTP_201_CREATED,
                        headers=response_headers)
