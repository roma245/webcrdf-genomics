from rest_framework import serializers

from .models import *
from .pagination import PaginatedRelationField, RelationPaginator


class UserProfileListSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='ira_app:userprofile-detail',
        lookup_field='pk'
    )

    class Meta:
        model = UserProfile
        fields = ('url', 'name', 'created_at', 'updated_at')


class UserProfileSerializer(serializers.ModelSerializer):
    jobs = serializers.HyperlinkedRelatedField(
        allow_null=True,
        many=True,
        queryset=Job.objects.all(),
        view_name='ira_app:job-detail',
    )

    class Meta:
        model = UserProfile
        fields = ('url', 'user', 'website', 'picture',
                  'jobs', 'created_at', 'updated_at')


class JobListSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='ira_app:job-detail',
        lookup_field='pk'
    )

    class Meta:
        model = Job
        fields = ('url', 'user', 'sra_id', 'operation',
                  'status', 'created_at', 'updated_at')


class JobSerializer(serializers.ModelSerializer):
    user = serializers.HyperlinkedRelatedField(
        allow_null=True,
        queryset=UserProfile.objects.all(),
        view_name='ira_app:userprofile-detail',
    )

    class Meta:
        model = Job
        fields = ('user', 'name', 'sra_id', 'operation',
                  'data_fasta', 'data_vcf',
                  'result_file', 'result_file_phylo', 'status',
                  'created_at', 'updated_at')


class JobCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = ('sra_id', 'operation', 'data_fasta', 'data_vcf')

    def create(self, validated_data):
        sra_id = validated_data.get('sra_id')
        data_fasta = validated_data.get('data_fasta')
        data_vcf = validated_data.get('data_vcf')
        operation = validated_data.get('operation')
        if sra_id and (data_fasta or data_vcf):
            raise serializers.ValidationError('Choose only one way')
        if not (sra_id or data_fasta or data_vcf):
            raise serializers.ValidationError('Choose at least one way')
        if data_fasta and operation != OperationEnum.ANNOTATION.name:
            raise serializers.ValidationError('File and operation mismatched')
        if data_vcf and operation != OperationEnum.RESISTANCE.name:
            raise serializers.ValidationError('File and operation mismatched')
        return Job.objects.create(**validated_data)
