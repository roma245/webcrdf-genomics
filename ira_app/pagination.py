from rest_framework import pagination
from rest_framework.fields import MultipleChoiceField
from rest_framework.settings import api_settings


class RelationPaginator(pagination.PageNumberPagination):
    page_size = 5

    def __init__(self, page_query_param):
        self.page_query_param = page_query_param

    def get_paginated_response(self, data):
        return {
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'results': data
        }


class PaginatedRelationField(MultipleChoiceField):
    def __init__(self, serializer, filters=None, order_by=None, paginator=None,
                 queryset=None, page_query_param=None, order_param=None,
                 **kwargs):
        self.serializer = serializer
        if paginator is None:
            paginator = api_settings.DEFAULT_PAGINATION_CLASS

        self.paginator = paginator(page_query_param)
        self.order_param = order_param

        # Filters should be a dict, for example: {'pk': 1}
        self.filters = filters
        self.order_by = order_by

        if queryset is not None:
            choices = {(o.id, str(o)) for o in queryset}
        else:
            choices = {}
        super(PaginatedRelationField, self).__init__(
            allow_null=True,
            choices=choices,
            **kwargs
        )

    def to_representation(self, related_objects):
        request = self.context.get('request')
        order = request.GET.get(self.order_param)
        if self.filters:
            related_objects = related_objects.filter(**self.filters)
        if self.order_by and order:
            if order == 'asc':
                related_objects = related_objects.order_by(self.order_by)
            elif order == 'desc':
                related_objects = related_objects.order_by(
                    '-' + str(self.order_by))

        serializer = self.serializer(
            related_objects, many=True, context={'request': request}
        )
        paginated_data = self.paginator.paginate_queryset(
            queryset=serializer.data, request=request
        )
        result = self.paginator.get_paginated_response(paginated_data)
        return result
