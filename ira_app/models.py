import os
import re
from django.db import models
from django.contrib.auth.models import User

from .choices import StatusEnum, OperationEnum


class TimestampsMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        indexes = [
            models.Index(fields=['created_at']),
            models.Index(fields=['updated_at']),
        ]


class UserProfile(TimestampsMixin, models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    def __str__(self):
        return self.user.username


class Job(TimestampsMixin, models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    operation = models.CharField(max_length=50, choices=OperationEnum.choices(),
                                 default=OperationEnum.RESISTANCE)
    sra_id = models.CharField(max_length=100, blank=True, null=True)
    data_fasta = models.FileField(upload_to='data_vcf', blank=True, null=True)
    data_vcf = models.FileField(upload_to='data_vcf', blank=True, null=True)
    result_file = models.FileField(upload_to='result_data', null=True)
    result_file_phylo = models.FileField(upload_to='result_data', null=True)
    status = models.CharField(max_length=20, choices=StatusEnum.choices(),
                              null=True)
    func_stack = models.IntegerField(default=0, null=True)
    datetime = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name

    def data_name(self):
        data_name = None
        if self.data_fasta:
            data_name = os.path.basename(self.data_fasta.name)
        elif self.data_vcf:
            data_name = os.path.basename(self.data_vcf.name)
        return data_name

    def result_name(self):
        return os.path.basename(self.result_file.name)

    def result_phylo_name(self):
        return os.path.basename(self.result_file_phylo.name)

    def save(self, *args, **kwargs):
        if self.sra_id:
            self.name = self.sra_id
        else:
            self.name = re.split('[ .,\_\-\']+', self.data_name())[0]
        super(Job, self).save(*args, **kwargs)
