import subprocess

from .choices import OperationEnum, StatusEnum
from .models import Job
from .static.scripts.mutations import check_mutations
from . import settings


def get_fastq(**kwargs):
    job_id = kwargs['dag_run'].conf['job_id']
    job = Job.objects.get(id=job_id)
    if job.func_stack == 0:
        subprocess.check_call([
            f'{settings.IRA_APP_STATIC_DIR}/'
            f'bash_scripts/1_get_sra2fastq.sh',
            job.sra_id,
            f'{settings.IRA_APP_MEDIA_DIR}/',
            settings.FASTQ_DUMP
        ])
        job.func_stack = 1
        job.save()


def bwa_assembling(**kwargs):
    job_id = kwargs['dag_run'].conf['job_id']
    job = Job.objects.get(id=job_id)
    if job.func_stack == 1:
        subprocess.check_call([
            f'{settings.IRA_APP_STATIC_DIR}/'
            f'bash_scripts/2_fastq2bai_parallel.sh',
            job.sra_id,
            f'{settings.IRA_APP_MEDIA_DIR}/',
            f'{settings.IRA_APP_STATIC_DIR}/',
            settings.BWA,
            settings.SAMTOOLS
        ])
        job.func_stack = 2
        job.save()


def create_vcf(**kwargs):
    job_id = kwargs['dag_run'].conf['job_id']
    job = Job.objects.get(id=job_id)
    if job.func_stack == 2:
        subprocess.check_call([
            f'{settings.IRA_APP_STATIC_DIR}/'
            f'bash_scripts/3_bai2vcf_parallel.sh',
            job.sra_id,
            f'{settings.IRA_APP_MEDIA_DIR}/',
            f'{settings.IRA_APP_STATIC_DIR}/',
            settings.PILON_JAR,
            settings.JAVA,
            settings.BGZIP,
            settings.TABIX,
            settings.VCFTOOLS
        ])
        job.data_fasta = f'data_vcf/{job.sra_id}.fasta'
        job.data_vcf = f'data_vcf/{job.sra_id}_filter.recode.vcf'
        job.func_stack = 3
        job.save()


def branch(**kwargs):
    job_id = kwargs['dag_run'].conf['job_id']
    job = Job.objects.get(id=job_id)
    if job.func_stack == 3:
        operation = None
        if job.operation == OperationEnum.RESISTANCE.name:
            operation = 'analyse_resistance'
        elif job.operation == OperationEnum.ANNOTATION.name:
            operation = 'make_annotation'
        job.func_stack = 4
        job.save()
        return operation


def analyse_resistance(**kwargs):
    job_id = kwargs['dag_run'].conf['job_id']
    job = Job.objects.get(id=job_id)
    if job.func_stack == 4:
        result_file, result_file_phylo = check_mutations(
            job.id,
            job.data_vcf.file.name,
            f'{settings.IRA_APP_STATIC_DIR}/',
            f'{settings.IRA_APP_MEDIA_DIR}/'
        )
        job.result_file = result_file
        job.result_file_phylo = result_file_phylo
        job.func_stack = 5
        job.status = StatusEnum.SUCCEED
        job.save()


def make_annotation(**kwargs):
    job_id = kwargs['dag_run'].conf['job_id']
    job = Job.objects.get(id=job_id)
    if job.func_stack == 4:
        subprocess.check_call([
            f'{settings.IRA_APP_STATIC_DIR}/'
            f'bash_scripts/4_make_annotation.sh',
            job.name,
            job.data_fasta.file.name,
            f'{settings.IRA_APP_MEDIA_DIR}/',
            f'{settings.IRA_APP_STATIC_DIR}/',
            settings.PROKKA
        ])
        result_file = f'data_prokka/{job.name}.zip'
        job.result_file = result_file
        job.func_stack = 5
        job.status = StatusEnum.SUCCEED
        job.save()
