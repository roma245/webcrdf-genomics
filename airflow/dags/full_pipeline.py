from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import (PythonOperator,
                                               BranchPythonOperator)

import os
import sys
import django
sys.path.append(os.environ.get('IRA_BASE_DIR', 'ira_rest_api'))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'IRA_rest_api.settings')
django.setup()

from ira_app.tasks import (get_fastq, bwa_assembling,
                           create_vcf, analyse_resistance,
                           make_annotation, branch)

dag = DAG('full_pipeline', description='Start with SRA id.',
          schedule_interval=None, start_date=datetime(2017, 3, 20),
          catchup=False)


fastq_operator = PythonOperator(task_id='get_fastq', provide_context=True,
                                python_callable=get_fastq, dag=dag)

bwa_operator = PythonOperator(task_id='bwa_assembling', provide_context=True,
                              python_callable=bwa_assembling, dag=dag)

vcf_operator = PythonOperator(task_id='create_vcf', provide_context=True,
                              python_callable=create_vcf, dag=dag)

branching = BranchPythonOperator(task_id='branching', provide_context=True,
                                 python_callable=branch,
                                 dag=dag)

resistance_resistance = PythonOperator(task_id='analyse_resistance',
                                       provide_context=True,
                                       python_callable=analyse_resistance,
                                       dag=dag)

annotation_operator = PythonOperator(task_id='make_annotation',
                                     provide_context=True,
                                     python_callable=make_annotation,
                                     dag=dag)

fastq_operator >> bwa_operator >> vcf_operator >> branching
branching >> resistance_resistance
branching >> annotation_operator
