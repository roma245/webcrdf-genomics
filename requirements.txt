Django==2.1.7
django-filter==2.1.0
django-rest-swagger==2.2.0
djangorestframework==3.9.2
Pillow==5.4.1
psycopg2-binary==2.7.7
xlrd==1.2.0
