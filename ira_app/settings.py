import os
from django.conf import settings


# Common
LOCAL_HOST: 'str' = 'localhost'

# Airflow
AIRFLOW_HOME: 'str' = os.environ.get('AIRFLOW_HOME', 'airflow')
AIRFLOW_WEBSERVER: 'str' = os.environ.get('AIRFLOW_WEBSERVER',
                                          LOCAL_HOST)
DAG_URL: 'str' = (
    f'http://{AIRFLOW_WEBSERVER}:8080/api/'
    f'experimental/dags/full_pipeline/dag_runs'
)

# Hierarchy
BASE_DIR: 'str' = settings.BASE_DIR
AIRFLOW_DIR: 'str' = settings.AIRFLOW_DIR
IRA_APP_STATIC_DIR: 'str' = settings.IRA_APP_STATIC_DIR
IRA_APP_MEDIA_DIR: 'str' = settings.IRA_APP_MEDIA_DIR

# Bio tools
FASTQ_DUMP: 'str' = os.environ.get('FASTQ_DUMP', '')
BWA: 'str' = os.environ.get('BWA', '')
SAMTOOLS: 'str' = os.environ.get('SAMTOOLS', '')
JAVA: 'str' = os.environ.get('JAVA', '')
BGZIP: 'str' = os.environ.get('BGZIP', '')
TABIX: 'str' = os.environ.get('TABIX', '')
VCFTOOLS: 'str' = os.environ.get('VCFTOOLS', '')
PILON_JAR: 'str' = os.environ.get('PILON_JAR', f'{BASE_DIR}/')
PROKKA = os.environ.get('PROKKA', f'{os.environ.get("HOME")}/prokka/bin/')
