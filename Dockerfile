FROM python:3.6

WORKDIR /service

RUN pip install --upgrade pip
ADD requirements.txt .
RUN pip install -r requirements.txt

EXPOSE 8000
