from django.apps import AppConfig


class IraAppConfig(AppConfig):
    name = 'ira_app'
