from django.urls import path, include
from rest_framework.routers import DefaultRouter

from ira_app.views import *


app_name = 'ira_app'

router = DefaultRouter()
router.register('userprofiles', UserProfileViewSet)
router.register('jobs', JobViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
