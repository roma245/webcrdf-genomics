import re
import numpy as np
import pandas as pd
from pathlib import Path


def get_mutations(file_path):
    with open(file_path, 'r') as file:
        for _ in range(31):
            next(file)
        mutations = []
        for line in file:
            line_list = line.split()
            alt = line_list[4]
            if alt != '.':
                pos = line_list[1]
                ref = line_list[3]
                mutations.append((pos, str(ref).lower(), str(alt).lower()))
        return mutations


def check_res(file_path, mutations):
    xls = pd.ExcelFile(file_path)
    df = pd.read_excel(xls, 'internal resistance SNPs')
    res_data = []
    for line in mutations:
        pos = df['Variant position genome start'] == int(line[0])
        ref = df['WT base'] == line[1]
        alt = df['Var. base'] == line[2]
        rows = df[pos & ref & alt]
        if rows.size > 0:
            res_data.extend(rows.values)
    return res_data


def create_report(file_path, file_path_phylo, res_data, media_dir):
    header = [
        'Variant position genome start', 'Variant position genome stop',
        'Var. type', 'Number', 'WT base', 'Var. base', 'Region', 'Gene ID',
        'Gene Name', 'Gene start', 'Gene stop', 'Gene length', 'Dir.',
        'WT AA', 'Codon nr.', 'Codon nr. E. coli', 'Var. AA', 'AA change',
        'Codon change', 'Variant position gene start',
        'Variant position gene stop', 'Antibiotic', 'Reference PMID',
        'High Confidence SNP', 'Linage'
    ]
    res_data_phylo = []
    res_data_anti = []
    try:
        for line in res_data:
            if 'phylo' in line[21]:
                line = np.append(line, re.split('[()]', line[21])[1])
                res_data_phylo.append(line)
            else:
                res_data_anti.append(line)
    except Exception as e:
        print(e)
    choose_col = [0, 2, 4, 5, 7, 17, 21, 22, 23]
    df = pd.DataFrame(res_data_anti)
    df = df[choose_col]
    df.to_csv(
        f'{media_dir}/{file_path}', sep='\t',
        header=list(header[i] for i in choose_col),
        encoding='utf-8', index=False
    )
    choose_col_phylo = [0, 2, 4, 5, 7, 17, 22, 24]
    df_phylo = pd.DataFrame(res_data_phylo)
    df_phylo = df_phylo[choose_col_phylo]
    df_phylo.to_csv(
        f'{media_dir}/{file_path_phylo}', sep='\t',
        header=list(header[i] for i in choose_col_phylo),
        encoding='utf-8', index=False
    )
    return file_path, file_path_phylo


def check_mutations(job_id, data_vcf_name, static_dir, media_dir):
    resist_file = (
        f'{static_dir}/resistance/'
        f'Resi-List-Master.v29_w_H37Rv_NC000962.3_reference.xlsx'
    )
    result_file = f'result_data/job_report{str(job_id)}.csv'
    result_file_phylo = f'result_data/job_report{str(job_id)}_phylo.csv'
    Path(f'{media_dir}/result_data').mkdir(parents=True, exist_ok=True)
    vcf_mutations = get_mutations(data_vcf_name)
    result = check_res(resist_file, vcf_mutations)
    return create_report(result_file, result_file_phylo, result, media_dir)
