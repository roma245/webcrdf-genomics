# ira-rest-api
IRA web-service allow you to check a given genome sequence on resistance to different drugs according to the mutations.

To install software via Docker do the following:
- install docker-ce, docker-engine, docker-compose and all required packages for your system
- create .env empty files in folder where Dockerfile exists
- run 'docker-compose -f docker-compose.yml -f airflow/docker-compose.airflow.yml up -d'
- start docker


## Setup
First, you should create 2 postgres databases
as it is written in settings and airflow.cfg files.

Setup your virtual environment and run:
```bash
pip install -r requirements.txt
```
Then you need to install redis as broker and result_backend
for celery. Use this guide https://redis.io/topics/quickstart
Run redis server:
```bash
redis-server --port 6378
```

Next, provide env var in EVERY bash you run Airflow:
```bash
export AIRFLOW_HOME=airflow/
```
And run each command in separate terminals (or in background):
```bash
airflow webserver
airflow scheduler
airflow worker
```
Also run django runserver. By default it's hosted on `localhost:8000`.

### Required tools
- `sra toolkit` - for using fastq-dump
- `bwa` - Burrows-Wheeler Aligner
- `samtools` - prepare aligned data to pilon
- `java` - run pilon-1.22.jar
- `pilon` - improve assemblies (required pilon-1.22.jar
            in root directory, or provide path 
            with env var)
- `tabix` - compress and index .vcf file
- `vcftools` - filter .vcf file

If you want to provide path to your tools directory
 use this pattern: `path/to/your/tool/directory/`
 
## Usage

To view Airflow UI go to `0.0.0.0:8080`
