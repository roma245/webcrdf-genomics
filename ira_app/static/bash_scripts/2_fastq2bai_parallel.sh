#!/bin/bash

cd ${2}
mkdir -p data_bam

#  Align to reference using BWA
${4}bwa index -a bwtsw ${3}'reference/H37Rv_NC000962.3_Reference.fasta'
${4}bwa mem ${3}'reference/H37Rv_NC000962.3_Reference.fasta' \
            ''${2}'data_fastq/'${1}'_1.fastq' \
            ''${2}'data_fastq/'${1}'_2.fastq' \
            -o ''${2}'data_bam/'${1}'_paired.sam'

#  Convert sam files to bam files using samtools.
${5}samtools view -bS ''${2}'data_bam/'${1}'_paired.sam' \
                  -o ''${2}'data_bam/'${1}'_paired.bam'

#  Sort the bam file using samtools
${5}samtools sort ''${2}'data_bam/'${1}'_paired.bam' \
                  -o ''${2}'data_bam/'${1}'_paired_sorted.bam' \
                  -o ''${2}'data_bam/'${1}'_sorted.bam'

#  Index the sorted bam file using samtools
${5}samtools index ''${2}'data_bam/'${1}'_sorted.bam'
