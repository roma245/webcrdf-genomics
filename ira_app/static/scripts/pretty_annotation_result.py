import csv
import sys

headers = ['locus_tag', 'ftype', 'start_pos', 'end_pos', 'length_bp',
           'gene', 'EC_number', 'COG', 'product']


def create_report(annotation_dir, prefix):
    gff = open(annotation_dir + '/' + prefix + '.gff')
    tsv = open(annotation_dir + '/' + prefix + '.tsv')
    report_file = open(annotation_dir + '/' + prefix + '_report.tsv', 'w')
    gff_reader = csv.reader(gff, delimiter='\t')
    tsv_reader = csv.reader(tsv, delimiter='\t')
    next(gff_reader)
    next(gff_reader)
    next(tsv_reader)
    report_writer = csv.writer(report_file, delimiter='\t')
    report_writer.writerow(headers)
    for tsv_line in tsv_reader:
        gff_line = next(gff_reader)
        report_line = tsv_line
        report_line.insert(2, gff_line[3])
        report_line.insert(3, gff_line[4])
        report_writer.writerow(report_line)
    gff.close()
    tsv.close()
    report_file.close()


if __name__ == '__main__':
    if len(sys.argv) == 3:
        annotation_dir = sys.argv[1]
        prefix = sys.argv[2]
        create_report(annotation_dir, prefix)
